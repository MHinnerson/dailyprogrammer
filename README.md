								Daily Programmer Solutions that I (Mattias H.) has made							
--------------------------------------------------------------------------------------------------------------------------------------------------

1: Overview

This is a repository containing Daily Programmer assignments that I've been exercising my programming on. 

There are many sites tht has a concept of daily programming and it basically mean that they post an programming execise once every day with the
focus of getting new coding experience (or just repeating things you already own). The reason behind it is just to have fun coding. The difficulty
of the exercises varies between Easy, Medium, or Hard. This repo here on my BitBucket is the solutions I've made for various daily programming exercises.


