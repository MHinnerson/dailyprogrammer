import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * [easy] Word Funnel
 * 
 * https://www.reddit.com/r/dailyprogrammer/comments/98ufvz/20180820_challenge_366_easy_word_funnel_1/
 * 
 * date: 2018-11-23
 * @author mattias
 *
 */
public class WordFunnel {

	public String firstWord;
	public String secondWord;
	
	public WordFunnel(){
		
		System.out.println("Please write first word");
		BufferedReader bufRead = new BufferedReader(new InputStreamReader(System.in));
		try {
			firstWord = bufRead.readLine();
			System.out.println("Please write second word");
			secondWord = bufRead.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void solve(){
		
		String secondWordParts = secondWord;
		String existedInBothString = "";
		int skipped = 0;
		
		for(int i=0; i < firstWord.length() ; i++){
			
			if(secondWordParts.contains("" + firstWord.charAt(i))){
				existedInBothString = existedInBothString + firstWord.charAt(i);
				
				int cutAwayIndex = secondWordParts.indexOf(secondWordParts);
				String firstPart = secondWordParts.substring(0, cutAwayIndex);
				String secondPart = secondWordParts.substring(cutAwayIndex + 1);
				secondWordParts = firstPart + secondPart;
			}else{
				skipped++;
			}
		}
		
		if((skipped == 1) && (existedInBothString.equals(secondWord))){				
			System.out.println("funnel(\"" + firstWord + "\", \"" + secondWord + "\") => true");			
		}else{
			System.out.println("funnel(\"" + firstWord + "\", \"" + secondWord + "\") => false");					
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		WordFunnel wFun = new WordFunnel();
		wFun.solve();
	}
}
